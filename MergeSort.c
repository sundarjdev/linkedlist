#include <stdio.h>
#include <stdlib.h>

struct node {
    int val;
    struct node *next;
};

void push(struct node **headref, int val)
{
    struct node *node = malloc(sizeof(struct node));
    node->val = val;
    node->next = *headref;
    *headref = node;
}

void printlist(struct node *head)
{
    struct node *curr = head;
    while (curr) {
        printf("%d-->", curr->val);
        curr = curr->next;
    }
    printf("\n");
}

void FrontBackSplit(struct node *head, struct node **front, struct node **back)
{
    struct node *slow = NULL, *fast = NULL;
    slow = fast = head;

    while (fast) {
        if (fast->next == NULL || fast->next->next == NULL)
            break;

        slow = slow->next;
        fast = fast->next->next;
    }

    if (slow == fast) {
        if (fast->next == NULL) {
            printf("nothing to split. One element list\n");
        } else {
            *back = slow->next;
            slow->next = NULL;
            *front = head;
        }
    } else {
        *back = slow->next;
        slow->next = NULL;
        *front = head;
    }
}

struct node *SortedMerge(struct node *a, struct node *b)
{
    struct node dummy;
    struct node *tail = &dummy;

    dummy.val = 0;
    dummy.next = NULL;

    while (1) {
        if (a == NULL) {
            tail->next = b;
            break;
        } else if (b == NULL) {
            tail->next = a;
            break;
        } else {
            if (a->val < b->val) {
                struct node *next = a->next;

                a->next = tail->next;
                tail->next = a;
                tail = tail->next;

                a = next;
            } else {
                struct node *next = b->next;

                b->next = tail->next;
                tail->next = b;
                tail = tail->next;

                b = next;
            }
        }
    }

    return dummy.next;
}

void MergeSort(struct node **headref)
{
    struct node *head = *headref;
    struct node *front = NULL, *back = NULL;

    if (head == NULL || head->next == NULL)
        return;

    FrontBackSplit(head, &front, &back);
    MergeSort(&front);
    MergeSort(&back);

    *headref = SortedMerge(front, back);
}

void test(void)
{
    struct node *head = NULL;
    struct node *front = NULL, *back = NULL;

    for (int i = 0; i < 20; i++)
        push(&head, i);

    FrontBackSplit(head, &front, &back);
    printlist(front);
    printlist(back);

    struct node *list1 = NULL, *list2 = NULL, *list = NULL;
    push(&list1, 10);
    push(&list1, 8);
    push(&list1, 1);

    push(&list2, 11);
    push(&list2, 5);
    push(&list2, 2);

    printlist(list1);
    printlist(list2);

    list = SortedMerge(list1, list2);
    printlist(list);
}

int main(int argc, char *argv[])
{
    struct node *head = NULL;

#if 0
    test();
#endif

    for (int i = 0; i < atoi(argv[1]); i++)
        push(&head, i);

    push(&head, 20);
    push(&head, 100);
    push(&head, 101);
    push(&head, 65);
    push(&head, 19);
    push(&head, 11);
    push(&head, 10);

    printlist(head);

    MergeSort(&head);

    printlist(head);


    return 0;
}
