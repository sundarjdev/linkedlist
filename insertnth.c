#include <stdio.h>
#include <stdlib.h>

struct node {
    int val;
    struct node *next;
};

void push(struct node **headref, int val)
{
    struct node *node = malloc(sizeof(struct node));
    node->val = val;
    node->next = *headref;
    *headref = node;
}

void printlist(struct node *head)
{
    struct node *curr = head;

    while (curr) {
        printf("%d-->", curr->val);
        curr = curr->next;
    }

    printf("\n");
}

void insertnth(struct node **headref, int pos, int val)
{
    struct node *node = malloc(sizeof(struct node));
    struct node *curr = *headref;
    struct node *prev = curr;

    if (!pos) {  // insert at pos 0 - HEAD
        node->val = val;
        node->next = *headref; 
        *headref = node;
        return;
    }

    while (curr && pos) {
        prev = curr;
        curr = curr->next;
        --pos;
    }

    if (!curr && pos) {
        printf("List not big enough to insert at requested position\n");
        free(node);
        return;
    }

    node->val = val;
    node->next = curr;
    prev->next = node;
}

int main(void)
{
    struct node *head = NULL;
    struct node **tailref = &head;

    printlist(head);

    for (int i = 0; i < 10; i++) {
        insertnth(&head, i, i);
    }

#if 0
    for (int i = 0; i < 10; i++) {
        push(tailref, i);
        tailref = &((*tailref)->next);
    }
#endif

    printlist(head);

    insertnth(&head, 0, 100);
    printlist(head);

    insertnth(&head, 2, 99);
    printlist(head);

    insertnth(&head, 4, 98);
    printlist(head);

    insertnth(&head, 9, 97);
    printlist(head);

    return 0;
}
