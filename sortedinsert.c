#include <stdio.h>
#include <stdlib.h>

struct node {
    int val;
    struct node *next;
};

void push(struct node **headref, int val)
{
    struct node *node = malloc(sizeof(struct node));
    node->val = val;
    node->next = *headref;
    *headref = node;
}

void insertnth(struct node **headref, int pos, int val)
{
    if (pos == 0) { // insert at head
        push(headref, val);
    } else { // insert elsewhere
        struct node *curr = *headref;

        while (curr && pos) {
            --pos;
            if (pos == 0)
                push(&(curr->next), val);

            if ((curr = curr->next) == NULL) {
                printf("Index Out-Of-Bounds\n");
            }
        }
    }
}

void printlist(struct node *head)
{
    struct node *curr = head;

    while (curr) {
        printf("%d-->", curr->val);

        curr = curr->next;
    }

    printf("\n");
}

void sortedinsert1(struct node **headref, struct node *node)
{
    if (*headref == NULL || node->val <= (*headref)->val) { // insert at head
        node->next = *headref;
        *headref = node;
        return;
    } else { // insert in between or at end
        struct node *curr = *headref;

        while (curr) {
            if (curr->next == NULL) { // insert at end
                node->next = NULL;
                curr->next = node;
                return;
            } else if (node->val <= (curr->next)->val) { // insert after curr
                node->next = curr->next;
                curr->next = node;
                return;
            } else {
                curr = curr->next;
            }
        }
    }
}

void sortedinsert(struct node **headref, struct node *node)
{
    struct node **localref = headref;

    while (*localref && (*localref)->val < node->val)
        localref = &((*localref)->next);

    node->next = *localref;
    *localref = node;
}

int main(void)
{
    struct node *head = NULL;

    struct node newnode;
    struct node newnode1;
    newnode.val = 140;
    newnode.next = NULL;
    sortedinsert(&head, &newnode);

    printlist(head);

    for (int i = 0; i < 20; i++)
        insertnth(&head, i, i*2);

    printlist(head);

    newnode1.val = 141;
    newnode1.next = NULL;
    sortedinsert1(&head, &newnode1);

    printlist(head);

    return 0;
}
