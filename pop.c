#include <stdio.h>
#include <stdlib.h>

struct node {
    int val;
    struct node *next;
};

void push(struct node **headref, int val)
{
    struct node *node = (struct node *)malloc(sizeof(struct node));
    node->val = val;
    node->next = *headref;
    *headref = node;
}

void printlist(struct node *head)
{
    struct node *curr = head;

    while (curr) {
        printf("%d-->", curr->val);
        curr = curr->next;
    }

    printf("\n");
}

int pop(struct node **headref)
{
    int retval;

    if (*headref) {
        struct node *temp = *headref;
        *headref = (*headref)->next;

        retval = temp->val;
        free(temp);

        return retval;
    }

    return -1;
}

int main(int argc, char *argv[])
{
    struct node *head = NULL;

    if (argc > 2 || argc <= 1) {
        printf("Invalid command\n");
        return -1;
    }

    for (int i = 0; i < atoi(argv[1]); i++) {
        push(&head, i); // {5, 4, 3, 2, 1}
    }

    printlist(head);

    for (int i = 0; i < atoi(argv[1]); i++) {
        printf("%d\n", pop(&head));
        printlist(head);
    }

    return 0;
}
