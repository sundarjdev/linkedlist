#include <stdio.h>
#include <stdlib.h>

struct node {
    int val;
    struct node * next;
};

void printlist(struct node *head)
{
    struct node *curr = head;
    while (curr) {
        printf("%d--->", curr->val);
        curr = curr->next;
    }
    printf("\n");
}

void push(struct node **headref, int val)
{
    struct node *node = (struct node *)malloc(sizeof(struct node));
    node->val = val;
    node->next = *headref;
    *headref = node;
}

void append(struct node **refa, struct node **refb)
{
    struct node *curra = *refa;
    while (curra && curra->next)
        curra = curra->next;

    if (curra == NULL)
        *refa = *refb;
    else
        curra->next = *refb;

    *refb = NULL;
}

int main(void)
{
    struct node *head = NULL;
    struct node *head1 = NULL;

    for (int i = 0; i < 20; i++)
        push(&head, i);

    printlist(head);

    for (int i = 20; i < 40; i++)
        push(&head1, i);

    printlist(head1);

    append(&head, &head1);

    printlist(head);
    printlist(head1);

    return 0;
}
