#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    int N;
    scanf("%d", &N);

    int *a = malloc(N * sizeof(int));
    int act_sum = 0;

    for (int i = 0; i < N; i++) {
        scanf("%d", &a[i]);
        act_sum += a[i];
    }

    for (int i = 0; i < N; i++)
        printf("%d \n", a[i]);


    int d1, d2, d, f;

    d1 = a[1] - a[0];
    d2 = a[2] - a[1];

    if (d1 <= d2)
        d = d1;
    else
        d = d2;

    f = a[0];

    printf("a: %d, n: %d, d: %d\n", f, N+1, d);

    int sum = (N+1)/2 * ((2*f) + (N)*d);

    printf("Mising term is %d\n", sum - act_sum);

    free(a);

    return 0;
}
