#include <stdio.h>
#include <stdlib.h>

struct node {
    int val;
    struct node *next;
};

void push(struct node **headref, int val)
{
    struct node *node = malloc(sizeof(struct node));
    node->val = val;
    node->next = *headref;
    *headref = node;
}

void printlist(struct node *head)
{
    struct node *curr = head;
    while (curr) {
        printf("%d-->", curr->val);
        curr = curr->next;
    }
    printf("\n");
}

void MoveNode(struct node **destref, struct node **srcref)
{
    if (*srcref == NULL) {
        printf("Empty src list\n");
        return;
    }

    struct node *temp = *srcref;
    *srcref = (*srcref)->next;

    temp->next = *destref;
    *destref = temp;
}

int main(void)
{
    struct node *head1 = NULL;
    struct node *head2 = NULL;

    for (int i = 0; i < 5; i++) {
        push(&head1, i);
        push(&head2, i * 2);
    }

    printf("Before list:\n");
    printlist(head1);
    printlist(head2);

    MoveNode(&head1, &head2);

    printf("After list:\n");
    printlist(head1);
    printlist(head2);

    return 0;
}
