#include <stdio.h>
#include <stdlib.h>

struct node {
    int val;
    struct node *next;
};

void push(struct node **headref, int val)
{
    struct node *node = (struct node *)malloc(sizeof(struct node));

    node->val = val;
    node->next = *headref;
    *headref = node;
}

void deletelist(struct node **headref)
{
    struct node *curr = *headref;

    while (curr) {
        struct node *temp = curr;
        curr = curr->next;
        free(temp);
    }

    *headref = NULL;
}

void printlist(struct node *head)
{
    struct node *curr = head;

    while (curr) {
        printf("%d-->", curr->val);
        curr = curr->next;
    }
    printf("\n");
}

int main(void)
{
    struct node *head = NULL;
    struct node **headref = &head;

    for (int i = 0; i < 30; i++) {
        push(headref, i); // {29, 28, 27, ... 3, 2, 1}
    }

    printlist(head);
    deletelist(&head);
    printlist(head);

    return 0;
}
