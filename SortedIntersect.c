#include <stdio.h>
#include <stdlib.h>

struct node {
    int val;
    struct node *next;
};

void push(struct node **headref, int val)
{
    struct node *node = malloc(sizeof(struct node));
    node->val = val;
    node->next = *headref;
    *headref = node;
}

void printlist(struct node *head)
{
    struct node *curr = head;
    while (curr) {
        printf("%d-->", curr->val);
        curr = curr->next;
    }
    printf("\n");
}

struct node *SortedIntersect(struct node *a, struct node *b)
{
    struct node *head = NULL;
    struct node **localref = &head;

    while (1) {
        if (a == NULL || b == NULL)
            break;
        
        if (a->val != b->val) {
            if (a->val < b->val)
                a = a->next;
            else
                b = b->next;
        } else {
            struct node *node = malloc(sizeof(struct node));
            node->val = a->val;
            node->next = *localref;
            *localref = node;

            localref = &((*localref)->next);

            a = a->next;
            b = b->next;
        }
    }

    return head;
}

int main(void)
{
    struct node *a = NULL, *b = NULL;
    struct node **a_ref = &a;
    struct node **b_ref = &b;

    for (int i = 5; i < 15; i++) {
        push(a_ref, i);
        a_ref = &((*a_ref)->next);
    }

    for (int i = 0; i < 10; i++) {
        push(b_ref, i);
        b_ref = &((*b_ref)->next);
    }

    printlist(a);
    printlist(b);

    printlist(SortedIntersect(a, b));

    return 0;
}
