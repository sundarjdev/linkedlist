#include <stdio.h>
#include <stdlib.h>

struct node {
    int val;
    struct node *next;
};

void push(struct node **localref, int val)
{
    struct node *node = malloc(sizeof(struct node));
    node->val = val;
    node->next = NULL;
    *localref = node;
}

int count(struct node *head, int val)
{
    struct node *curr = head;
    int count = 0;

    while (curr) {
        if (curr->val == val)
            count++;

        curr = curr->next;
    }

    return count;
}

int main(void)
{
    struct node *head = NULL;
    struct node **localref = &head;

    for (int i = 0; i < 10; i++) {
        push(localref, i);
        localref = &((*localref)->next);
    }

    push(localref, 2);
    localref = &((*localref)->next);

    push(localref, 2);
    localref = &((*localref)->next);

    printf("%d count is: %d\n", 4, count(head, 4));

    return 0;
}
