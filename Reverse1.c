#include <stdio.h>
#include <stdlib.h>

struct node {
    int val;
    struct node *next;
};

void push(struct node **headref, int val)
{
    struct node *node = (struct node *)malloc(sizeof(struct node));
    node->val = val;
    node->next = *headref;
    *headref = node;
}

void printlist(struct node *head)
{
    struct node *node = head;

    while (node) {
        printf("%d-->", node->val);
        node = node->next;
    }
    printf("\n");
}

void Reverse(struct node **headref)
{
    struct node *head = *headref;
    struct node *r_head = NULL;

    while (head != NULL) {
        struct node *next = head->next;
        head->next = r_head;
        r_head = head;
        head = next;
    }

    *headref = r_head;
}

void RecursiveReverse(struct node **headref)
{
    struct node *head = *headref;

    if (head == NULL)
        return;

    struct node *next = (*headref)->next;
    *headref = next;
    if (next->next != NULL) {
        head->next = NULL;
        RecursiveReverse(headref);
    }
    next->next = head;
}

int main(void)
{
    struct node *head = NULL;
    
    for (int i = 0; i < 10; i++)
        push(&head, i);

    printlist(head);

    Reverse(&head);

    printlist(head);

    RecursiveReverse(&head);

    printlist(head);

    return 0;
}
