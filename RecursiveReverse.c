#include <stdio.h>
#include <stdlib.h>

struct node {
    int val;
    struct node *next;
};

void push(struct node **headref, int val)
{
    struct node *node = malloc(sizeof(struct node));
    node->val = val;
    node->next = *headref;
    *headref = node;
}

void printlist(struct node *head)
{
    struct node *curr = head;

    if (curr) {
        printf("%d-->", curr->val);
        printlist(curr->next); // Recursively call printlist for remaining elements
    } else {    // base condition
        printf("\n");
    }
}

void RecursiveReverse(struct node **headref)
{
    struct node *first = *headref;
    struct node *rest = (*headref)->next;

    if (first == NULL)
        return;

    if (rest == NULL)
        return;

    RecursiveReverse(&rest);

    first->next->next = first;
    first->next = NULL;

    *headref = rest;
}

int main(void)
{
    struct node *head = NULL;

    for (int i = 0; i < 10; i++)
        push(&head, i);

    printlist(head);

    RecursiveReverse(&head);

    printlist(head);

    return 0;
}
