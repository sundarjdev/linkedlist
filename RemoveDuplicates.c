#include <stdio.h>
#include <stdlib.h>

struct node {
    struct node *next;
    int val;
};

void push(struct node **headref, int val)
{
    struct node *node = malloc(sizeof(struct node));
    node->val = val;
    node->next = *headref;
    *headref = node;
}

void printlist(struct node *node)
{
    while (node) {
        printf("%d-->", node->val);
        node = node->next;
    }
    printf("\n");
}

void RemoveDuplicates(struct node *head)
{
    struct node *curr = head;

    while (curr && curr->next) {
        if (curr->val != (curr->next)->val)
            curr = curr->next;
        else {
            struct node *temp = curr->next;
            curr->next = (curr->next)->next;
            free(temp);
        }
    }
}

int main(void)
{
    struct node *head = NULL;

    push(&head, 10);
    push(&head, 10);
    push(&head, 11);
#if 0
    push(&head, 12);
    push(&head, 12);
    push(&head, 13);
    push(&head, 13);
    push(&head, 13);
    push(&head, 13);
    push(&head, 13);
    push(&head, 14);
    push(&head, 15);
    push(&head, 16);
    push(&head, 16);
    push(&head, 16);
    push(&head, 16);
    push(&head, 16);
    push(&head, 16);
#endif
    printlist(head);

    RemoveDuplicates(head);

    printlist(head);

    return 0;
}
