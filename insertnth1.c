#include <stdio.h>
#include <stdlib.h>

struct node {
    int val;
    struct node *next;
};

void push(struct node **headref, int val)
{
    struct node *node = malloc(sizeof(struct node));

    node->val = val;
    node->next = *headref;
    *headref = node;
}

void printlist(struct node *head)
{
    struct node *curr = head;
    
    while (curr) {
        printf("%d-->", curr->val);
        curr = curr->next;
    }

    printf("\n");
}

void insertnth(struct node **headref, int pos, int val)
{
    if (pos == 0) { // insert at head
        push(headref, val);
    } else { // insert elsewhere
        struct node *curr = *headref;

        while (curr && pos) {
            if (--pos == 0x0) { // if we reached pos, insert
                push(&(curr->next), val);
            }

            if ((curr = curr->next) == NULL) {
                printf("Index out of bounds\n");
                return;
            }
        }
    }
}

int main(void)
{
    struct node *head = NULL;

    for (int i = 0; i < 10; i++) {
        //push(&head, i);
        insertnth(&head, i, i);
    }

    printlist(head);

    insertnth(&head, 12, 12);

    for (int i = 10; i < 20; i++)
        insertnth(&head, i, i);

    printlist(head);

    return 0;
}
