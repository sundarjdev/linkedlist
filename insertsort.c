#include <stdio.h>
#include <stdlib.h>

struct node {
    int val;
    struct node *next;
};

void push(struct node **headref, int val)
{
    struct node *node = malloc(sizeof(struct node));
    node->val = val;
    node->next = *headref;
    *headref = node;
}

void printlist(struct node *head)
{
    struct node *curr = head;

    while (curr) {
        printf("%d-->", curr->val);
        curr = curr->next;
    }

    printf("\n");
}

void sortedinsert(struct node **headref, struct node *node)
{
    while (*headref && (*headref)->val < node->val) {
        headref = &((*headref)->next);
    }

    node->next = *headref;
    *headref = node;
}

void insertsort(struct node **headref)
{
    struct node *newhead = NULL;

    struct node *curr = *headref;
    struct node *next;
    while (curr) {
        next = curr->next;
        sortedinsert(&newhead, curr);
        printlist(newhead);
        curr = next;
    }

    *headref = newhead;
}

int main(void)
{
    struct node *head = NULL;

    for (int i = 0; i < 10; i++) {
        push(&head, i);
    }

    printlist(head);

    struct node *node;
    for (int i = 20; i > 9; i--) {
        node = (struct node *)malloc(sizeof(struct node));
        node->val = i;
        node->next = NULL;

        sortedinsert(&head, node);
    }

    printlist(head);
    insertsort(&head);
    printlist(head);

    return 0;
}
