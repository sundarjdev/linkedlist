#include <stdio.h>
#include <stdlib.h>

struct node {
    int val;
    struct node *next;
};

void push(struct node **localref, int val)
{
    struct node *node = malloc(sizeof(struct node));
    node->val = val;
    node->next = NULL;
    *localref = node;
}

void buildlist(struct node **localref, int num)
{
    for (int i = 0; i < num; i++) {
        push(localref, i);
        localref = &((*localref)->next);
    }
}

int getnth(struct node *head, int index)
{
    struct node *curr = head;
    int count = 0;

    while (curr) {
        if (count == index)
            break;

        count++;
        curr = curr->next;
    }

    if (count != index)
        return -1; 

    return (curr->val);
}

int main(int argc, char *argv[])
{
    struct node *head = NULL;

    if (argc > 3) {
        printf("Command not supported\n");
        return -1;
    }

    buildlist(&head, atoi(argv[1]));

    printf("list[%d]: %d\n", atoi(argv[2]), getnth(head, atoi(argv[2])));

    return 0;
}
