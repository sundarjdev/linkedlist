#include <stdio.h>
#include <stdlib.h>

struct node {
    int val;
    struct node *next;
};

void push(struct node **headref, int val)
{
    struct node *node = malloc(sizeof(struct node));
    node->val = val;
    node->next = *headref;
    *headref = node;
}

void printlist(struct node *head)
{
    struct node *curr = head;
    while (curr) {
        printf("%d-->", curr->val);
        curr = curr->next;
    }
    printf("\n");
}

void Reverse(struct node **headref)
{
    if (*headref == NULL) {
        printf("Empty list\n");
        return;
    }

    struct node *next = (*headref)->next;
    (*headref)->next = NULL;

    while (next) {
        struct node *temp = next->next;

        next->next = *headref;
        *headref = next;

        next = temp;
    }
}

void Reverse1(struct node **headref)
{
    struct node *back = NULL;
    struct node *middle = *headref;
    struct node *front = middle->next;

    while (1) {
        middle->next = back;

        if (front == NULL)
            break;
        else {
            back = middle;
            middle = front;
            front = front->next;
        }
    }

    *headref = middle;
}

int main(void)
{
    struct node *head = NULL;
    for (int i = 0; i < 10; i++)
        push(&head, i);

    push(&head, 100);
    push(&head, 0);
    push(&head, 99);
    push(&head, 19);
    printlist(head);

    Reverse(&head);

    printlist(head);

    Reverse1(&head);

    printlist(head);

    return 0;
}
