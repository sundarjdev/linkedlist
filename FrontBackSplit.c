#include <stdio.h>
#include <stdlib.h>

struct node {
    int val;
    struct node *next;
};

void push(struct node **headref, int val)
{
    struct node *node = malloc(sizeof(struct node));
    node->val = val;
    node->next = *headref;
    *headref = node;
}

void printlist(struct node *curr)
{
    while (curr) {
        printf("%d-->", curr->val);
        curr = curr->next;
    }
    printf("\n");
}

/* Uses slow and fast pointer to iterate the linked list */
void FrontBackSplit(struct node *head, struct node **frontref, struct node **backref)
{
    struct node *slowptr, *fastptr;

    slowptr = fastptr = head;

    if (fastptr == NULL) {
        printf("List empty\n");
        return;
    }

    *frontref = head; // Front Ref set to HEAD
    while (fastptr->next && (fastptr->next)->next) {
        slowptr = slowptr->next;
        fastptr = (fastptr->next)->next;
    }

    if (fastptr == slowptr && fastptr->next == NULL) { // Single element list
        printf("One element list. Nothing to split\n");
        return;
    }

    *backref = slowptr->next; // Backref
    slowptr->next = NULL; // Split
}

int main(int argc, char *argv[])
{
    struct node *head = NULL;

    if (argc != 2) {
        printf("Usage: ./FrontBackSplit <number of nodes>\n");
        return -1;
    }

    for (int i = 0; i < atoi(argv[1]); i++)
        push(&head, i);

    printlist(head);

    struct node *front, *back;
    FrontBackSplit(head, &front, &back);

    printf("First List: \n");
    printlist(front);

    printf("Second List: \n");
    printlist(back);

    return 0;
}
