#include <stdio.h>
#include <stdlib.h>

struct node {
    int val;
    struct node *next;
};

void push(struct node **headref, int val)
{
    struct node *node = malloc(sizeof(struct node));
    node->val = val;
    node->next = *headref;
    *headref = node;
}

void printlist(struct node *head)
{
    struct node *curr = head;
    while (curr) {
        printf("%d-->", curr->val);
        curr = curr->next;
    }
    printf("\n");
}
struct node *ShuffleMerge(struct node *list1, struct node *list2)
{
    struct node *head = NULL;
    struct node **refptr = &head;

    while (1) {
        if (list1) {
            struct node *next = list1->next;
            list1->next = NULL;
            *refptr = list1;
            refptr = &(list1->next);

            list1 = next;
        }

        if (list2) {
            struct node *next = list2->next;
            list2->next = NULL;
            *refptr = list2;
            refptr = &((*refptr)->next);

            list2 = next;
        }

        if (list1 == NULL) {
            *refptr = list2;
            break;
        }
        
        if (list2 == NULL) {
            *refptr = list1;
            break;
        }
    }

    return head;
}

int main(void)
{
    struct node *list1 = NULL;
    struct node *list2 = NULL;
    struct node *list = NULL; // list = list1 + list2

    for (int i = 0; i < 10; i++)
        push(&list1, i);

    push(&list2, 100);
    push(&list2, 101);
    push(&list2, 102);
    push(&list2, 103);
    push(&list2, 104);
    push(&list2, 105);

    printlist(list1);
    printlist(list2);

    list = ShuffleMerge(list1, list2);

    printlist(list);

    return 0;
}
