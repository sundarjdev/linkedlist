#include <stdio.h>
#include <stdlib.h>

struct node {
    int val;
    struct node *next;
};

struct node *build123()
{
    struct node *head = malloc(sizeof(struct node));
    struct node *second = malloc(sizeof(struct node));
    struct node *third = malloc(sizeof(struct node));

    head->val = 1;
    head->next = second;

    second->val = 2;
    second->next = third;

    third->val = 3;
    third->next = NULL;

    return head;
}

void push(struct node **head, int val)
{
    struct node *new = malloc(sizeof(struct node));
    new->val = val;
    new->next = *head;
    *head = new;
}

int main(void)
{
    int **a;

    a = (int **)malloc(10 * sizeof(int *));
    for (int i = 0; i < 10; i++)
        a[i] = (int *)malloc(10 * sizeof(int));

    int num = 0;

    for (int i = 0; i < 10; i++)
        for (int j = 0; j < 10; j++)
            a[i][j] = num++;

    for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 10; j++) {
            printf("%d\t", a[i][j]);
        }
        printf("\n");
    }

    for (int i = 0; i < 10; i++)
        free(a[i]);

    free(a);

    struct node *head = NULL;
    push(&head, 1);
    push(&head, 2);
    push(&head, 3);
    push(&head, 4);
    push(&head, 5);

    struct node *curr = head;
    while(curr) {
        printf("%d--->", curr->val);
        curr = curr->next;
    }
    printf("\n");

    push(&head, 6);

    curr = head;
    while(curr) {
        printf("%d--->", curr->val);
        curr = curr->next;
    }
    printf("\n");

    /* push at tail end of list */
    curr = head;
    while (curr->next != NULL) {
        curr = curr->next;
    }

    push(&curr->next, 0);
    push(&curr->next, 1);
    push(&curr->next, 2);

    curr = head;
    while(curr) {
        printf("%d--->", curr->val);
        curr = curr->next;
    }
    printf("\n");
    while(head) {
        struct node *temp = head;
        head = head->next;
        free(temp);
    }

    return 0;
}
