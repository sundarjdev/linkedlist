#include <stdio.h>
#include <stdlib.h>

struct node {
    int val;
    struct node *next;
};

void push(struct node **headref, int val)
{
    struct node *node = malloc(sizeof(struct node));
    node->val = val;
    node->next = *headref;
    *headref = node;
}

void printlist(struct node *head)
{
    struct node *curr = head;
    while(curr) {
        printf("%d-->", curr->val);
        curr = curr->next;
    }
    printf("\n");
}

void AlternatingSplit(struct node *head, struct node **list1, struct node **list2)
{
    struct node *curr = head;

    while (curr) {
        struct node *curr_next = curr->next;
        curr->next = *list1;
        *list1 = curr;

        curr = curr_next;
        if (curr) {
            curr_next = curr->next;

            curr->next = *list2;
            *list2 = curr;

            curr = curr_next;
        }
    }
}

void AlternatingSplit1(struct node *head, struct node **list1, struct node **list2)
{
    struct node *curr = head;
    struct node dummy1, dummy2;
    struct node *tail1 = &dummy1;
    struct node *tail2 = &dummy2;

    dummy1.next = NULL;
    dummy2.next = NULL;

    while (curr) {
        struct node *curr_next = curr->next;
        curr->next = tail1->next;
        tail1->next = curr;
        tail1 = curr;

        curr = curr_next;

        if (curr) {
            curr_next = curr->next;

            curr->next = tail2->next;
            tail2->next = curr;
            tail2 = curr;

            curr = curr_next;
        }
    }

    *list1 = dummy1.next;
    *list2 = dummy2.next;
}

int main(int argc, char *argv[])
{
    struct node *head1 = NULL;
    struct node *head2 = NULL;

    if (argc != 2) {
        printf("Invalid Usage\n");
        return -1;
    }

    for (int i = 0; i < atoi(argv[1]); i++) {
        push(&head1, i);
        push(&head2, i);
    }

    printlist(head1);

    struct node *list1 = NULL;
    struct node *list2 = NULL;
    AlternatingSplit(head1, &list1, &list2);

    printf("After AlternatingSplit: \n");
    printlist(list1);
    printlist(list2);

    printlist(head2);

    list1 = NULL;
    list2 = NULL;
    AlternatingSplit1(head2, &list1, &list2);

    printf("After AlternatingSplit1: \n");
    printlist(list1);
    printlist(list2);

    return 0;
}
