#include <stdio.h>
#include <stdlib.h>

struct node {
    int val;
    struct node *next;
};

void push(struct node **headref, int val)
{
    struct node *node = malloc(sizeof(struct node));
    node->val = val;
    node->next = *headref;
    *headref = node;
}

void printlist(struct node *head)
{
    struct node *curr = head;

    while (curr) {
        printf("%d-->", curr->val);
        curr = curr->next;
    }
    printf("\n");
}

struct node *SortedMerge(struct node *a, struct node *b)
{
    struct node *head = NULL;
    struct node **localref = &head;

    while(1) {
        if (a == NULL) {
            *localref = b;
            break;
        } else if (b == NULL) {
            *localref = a;
            break;
        } else {
            if (a->val < b->val) {
                struct node *next = a->next;

                a->next = *localref;
                *localref = a;
                localref = &((*localref)->next);

                a = next;
            } else {
                struct node *next = b->next;

                b->next = *localref;
                *localref = b;
                localref = &((*localref)->next);

                b = next;
            }
        }
    }

    return head;
}

int main(void)
{
    struct node *head1 = NULL;
    struct node **localref1 = &head1;
    struct node *head2 = NULL;
    struct node **localref2 = &head2;

    printf("trial 1:\n");
    for (int i = 0; i < 10; i += 2) {
        push(localref1, i);
        localref1 = &((*localref1)->next);
    }
    printlist(head1);

    for (int i = 1; i < 10; i += 2) {
        push(localref2, i);
        localref2 = &((*localref2)->next);
    }
    printlist(head2);

    struct node *list = NULL;
    list = SortedMerge(head1, head2);

    printlist(list);

    printf("trial 2:\n");
    head1 = head2 = NULL;
    list = NULL;
    localref1 = &head1;
    localref2 = &head2;

    for (int i = 0; i < 10; i++) {
        push(localref1, i);
        localref1 = &((*localref1)->next);
    }
    printlist(head1);
    printlist(head2);
    list = SortedMerge(head1, head2);
    printlist(list);

    printf("trial 3:\n");
    head1 = head2 = NULL;
    list = NULL;
    localref1 = &head1;
    localref2 = &head2;

    for (int i = 0; i < 10; i++) {
        push(localref1, i);
        localref1 = &((*localref1)->next);
    }
    printlist(head1);
    printlist(head2);
    list = SortedMerge(head1, head2);
    printlist(list);

    printf("trial 4:\n");
    head1 = head2 = NULL;
    list = NULL;
    localref1 = &head1;
    localref2 = &head2;

    for (int i = 0; i < 10; i++) {
        push(localref2, i);
        localref2 = &((*localref2)->next);
    }
    printlist(head1);
    printlist(head2);
    list = SortedMerge(head1, head2);
    printlist(list);

    printf("trial 5:\n");
    head1 = head2 = NULL;
    list = NULL;
    localref1 = &head1;
    localref2 = &head2;

    for (int i = 0; i < 10; i += 2) {
        push(localref1, i);
        localref1 = &((*localref1)->next);
    }
    push(&head2, 11);
    push(&head2, 10);
    push(&head2, 1);
    printlist(head1);
    printlist(head2);
    list = SortedMerge(head1, head2);
    printlist(list);

    return 0;
}
